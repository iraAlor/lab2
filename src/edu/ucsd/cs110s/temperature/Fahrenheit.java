/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author valor
 *
 */
public class Fahrenheit extends Temperature {

	public Fahrenheit(float t)
	{
	super(t);
	}
	public String toString()
	{
	// TODO: Complete this method
	return Float.toString(getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(((getValue() - 32)* 5)/9);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	
	
}
