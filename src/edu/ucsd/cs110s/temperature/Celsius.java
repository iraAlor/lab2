/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author valor
 *
 */
public class Celsius extends Temperature {
	
	public Celsius(float t)
	{
	super(t);
	}
	public String toString()
	{
	// TODO: Complete this method
	return Float.toString(getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(((getValue()*9)/5)+32);
	}
	
	

}
